#!/usr/bin/python

import battle.net.character

class BVRCharacter(battle.net.character.Character):
    """Specific character information for the battleground victory ration."""

    def __init__(self, realm=None, name=None):
        super(BVRCharacter, self).__init__(realm, name)
        self._bg_ratio = None
        return

    def _build_bg_ratio(self):
        """Return the matrix of BGs, victories and victory ratio for a
        character."""
        stats = self.stats_pvp_bg

        bgs = ['Alterac Valley', 'Battle for Gilneas', 'Arathi Basin',
                'Eye of the Storm', 'Strand of the Ancients', 'Twin Peaks',
                'Warsong Gulch', 'Isle of Conquest']

        result = {}
        for bg in bgs:
            battles = int(stats.get(bg + ' battles', 0))
            victories = int(stats.get(bg + ' victories', 0))

            ratio = 0
            if battles > 0:
                ratio = (float(victories) / float(battles)) * 100

            result[bg] = {
                    'victories': victories,
                    'battles': battles,
                    'ratio': ratio
                    }
        return result

    @property
    def bg_ratio(self):
        """Return the BG victory ratio for the character."""
        if not self._bg_ratio:
            self._bg_ratio = self._build_bg_ratio()
        return self._bg_ratio