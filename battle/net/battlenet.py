#!/usr/bin/python

import urllib2

def find_xpath(root, path):
    """Finds an XPath element "path" in the element "root", converting to the
    weird information ElementTree is getting from battle.net."""
    elements = path.split('/')
    path = []
    for el in elements:
        if not el.endswith(']'):
            path.append('{http://www.w3.org/1999/xhtml}'+el)
        else:
            # collect what we have, find the element, reset root and path
            this_element = el.split('[')
            # first part, without the 
            path.append('{http://www.w3.org/1999/xhtml}'+this_element[0])
            xpath = '/'.join(path)
            root = root.findall(xpath)

            pos = int(this_element[1][0:-1]) -1
            root = root[pos]

            path = []

    if len(path) > 0:
        xpath = '/'.join(path)
        root = root.find(xpath)

    return root


class BattleNet(object):
    """Base class for Battle.net requests."""

    def _get_pointer(self, url):
        """Request the URL. Returns the Request socket/file pointer."""
        self._log.debug('Requesting %s...', url)
        req = urllib2.Request(url)
        req.add_header('User-Agent', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.6; rv:2.0) Gecko/20100101 Firefox/4.0')
        req.add_header('Accept', 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8')
        content = urllib2.urlopen(req)
        return content

