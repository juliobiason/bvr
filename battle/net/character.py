#!/usr/bin/python

import logging
import battlenet

from xml.etree.ElementTree import ElementTree

class Character(battlenet.BattleNet):
    """Character information."""

    def __init__(self, realm=None, name=None):
        self.realm = (realm or '').replace("'", '').replace(' ', '-').lower()
        self.name = (name or '').replace("'", '').replace(' ', '-').lower()
        self._log = logging.getLogger('battle.net.character')
        self._stats_pvp_bg = None
        self._main = None
        return

    def _get_stats_pointer(self, stats):
        """Retrieve the character statistics. Returns the socket/file pointer
        as required by the ElementTree to be parsed."""
        url = 'http://us.battle.net/wow/en/character/%s/%s/statistic/%d' % (
                self.realm, self.name, stats)
        return self._get_pointer(url)

    def _get_main_pointer(self):
        """Retrieve the character main information. Returns the socket/file
        pointer as required by ElementTree to be parsed."""
        url = 'http://us.battle.net/wow/en/character/%s/%s/simple' % (
                self.realm, self.name)
        return self._get_pointer(url)

    def _get_stats_pvp_bg(self):
        """Parse the BG stats and return a dictionary with all information."""
        # PyQuery doesn't seem to like this piece of code. Since ElementTree
        # is part of the Python STL, we'll still use it for this.
        doc = ElementTree(file=self._get_stats_pointer(153))
        result = {}
        for element in doc.findall(
                '{http://www.w3.org/1999/xhtml}ul/'
                '{http://www.w3.org/1999/xhtml}li/'
                '{http://www.w3.org/1999/xhtml}dl'):

            stat_name = element.find('{http://www.w3.org/1999/xhtml}dt').text
            stat_count = element.find('{http://www.w3.org/1999/xhtml}dd').text

            stat_name = stat_name.strip().replace(',', '')
            stat_count = stat_count.strip().replace(',', '')

            if not stat_name or not stat_count:
                continue

            self._log.debug('%s = %s' % (stat_name, stat_count))
            result[stat_name] = stat_count
        return result

    def _get_main(self):
        """Parse the main information (guild name, class, race, title, 
        achivement points, total honorable kills and faction)"""
        doc = ElementTree(file=self._get_main_pointer())
        root = doc.getroot()

        header = battlenet.find_xpath(root, 'body/div/div[2]/div/div[2]/div')
        result = {
                'class': battlenet.find_xpath(root, "body/div/div[2]/div/div[2]/div/div/div/div/div/div/div/div[3]/a[3]").text.strip(),
                'level': battlenet.find_xpath(root, "body/div/div[2]/div/div[2]/div/div/div/div/div/div/div/div[3]/span/strong").text.strip(),
                'title': battlenet.find_xpath(root, "body/div/div[2]/div/div[2]/div/div/div/div/div/div/div/div[2]/div").text.strip(),
                'guild': battlenet.find_xpath(root, "body/div/div[2]/div/div[2]/div/div/div/div/div/div/div/div[2]/div[2]/a").text.strip(),
                'achievements': battlenet.find_xpath(root, "body/div/div[2]/div/div[2]/div/div/div/div/div/div/div/div[4]/a").text.strip(),
                'honorable-kills': battlenet.find_xpath(root, "body/div/div[2]/div/div[2]/div/div[2]/div[2]/div[2]/div[3]/div[2]/div/ul/li[2]/span[2]").text.strip(),
                'faction': 'alliance' if 'alliance' in header.attrib['class'] else 'horde'
                }
        return result
    
    @property
    def stats_pvp_bg(self):
        """Statistics for Battlegrounds (Statistics -> Player vs Player ->
        Battlegrounds."""
        if not self._stats_pvp_bg:
            self._stats_pvp_bg = self._get_stats_pvp_bg()
        return self._stats_pvp_bg

    @property
    def main(self):
        """Main character information."""
        if not self._main:
            self._main = self._get_main()
        return self._main
