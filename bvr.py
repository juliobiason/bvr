#!/usr/bin/python

import bvrcharacter
import optparse
import logging

def ratio(params):
    """Character victory ratio."""
    char = bvrcharacter.BVRCharacter(params.realm, params.char)
    battles = 0
    victories = 0
    for bg in char.bg_ratio:
        print '%-25s %4s %4s %6.2f' % (bg, char.bg_ratio[bg]['battles'], 
                char.bg_ratio[bg]['victories'],
                char.bg_ratio[bg]['ratio'])
        battles += float(char.bg_ratio[bg]['battles'])
        victories += float(char.bg_ratio[bg]['victories'])
    print '%25s %4d %4d %6.2f' % ('Total', battles, victories,
            ((victories / battles) * 100.0))
    return


def info(params):
    char = bvrcharacter.BVRCharacter(params.realm, params.char)
    for field in char.main:
        print '%-20s %s' % (field, char.main[field])


def main():
    # set up logging
    logging.basicConfig(level=logging.DEBUG)

    # avaialble commands (and callbacks)
    commands = {
        'ratio': ratio,
        'info': info
        }

    # command line parser
    parser = optparse.OptionParser()
    parser.add_option('-r', '--realm',
            help='Realm',
            dest='realm')
    parser.add_option('-c', '--character',
            help='Character',
            dest='char');

    (options, args) = parser.parse_args()

    # param checking
    if not options.realm:
        parser.error('Realm is required')
        return

    if not options.char:
        # required for now
        parser.error('Character is required')
        return

    if not args or len(args) != 1 or args[0] not in commands:
        all_comms = ', '.join(commands.keys())
        parser.error('Uknown command. Valid commands are: %s' % (all_comms))
        return

    commands[args[0]](options)

if __name__ == '__main__':
    main()